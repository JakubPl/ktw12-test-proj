package com.example.demo.service;

import org.springframework.stereotype.Component;

@Component
public class MySingletonBean {
    private String someValue;

    public String getSomeValue() {
        return someValue;
    }

    public void setSomeValue(String someValue) {
        this.someValue = someValue;
    }
}
