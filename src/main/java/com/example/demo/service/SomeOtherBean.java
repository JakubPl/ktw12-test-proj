package com.example.demo.service;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;

@RequestScope
@Component
public class SomeOtherBean {
    private final MyRequestBean myRequestBean;
    private HttpServletRequest request;

    public SomeOtherBean(MyRequestBean myRequestBean) {
        this.myRequestBean = myRequestBean;
    }

    public void printValue() {
        System.out.println(request.toString());
    }
}
