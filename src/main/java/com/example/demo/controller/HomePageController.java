package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;

@Controller
public class HomePageController {
    @GetMapping("home")
    public ModelAndView get(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("welcome");
        modelAndView.addObject("users", Arrays.asList((UserDetails) authentication.getPrincipal()));
        return modelAndView;
    }

}
