package com.example.demo.controller;

import com.example.demo.dto.LoginForm;
import com.example.demo.dto.UserForm;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    private UserService userService;

    public UserController(UserService myService) {
        this.userService = myService;
    }

    @GetMapping("register")
    public ModelAndView registerGet() {
        return new ModelAndView("createNewUser", "userForm", new UserForm());
    }

    @GetMapping("login")
    public ModelAndView loginGet() {
        return new ModelAndView("login", "loginForm", new LoginForm());
    }

    @PostMapping("register")
    public String registerPost(@ModelAttribute UserForm userForm) {
        userService.save(userForm.getLogin(), userForm.getPassword());
        return "redirect:/home";
    }
}
