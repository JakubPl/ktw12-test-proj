package com.example.demo.controller;

import com.example.demo.dto.MessageForm;
import com.example.demo.validator.MessageFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@RequestMapping("/message")
@Controller
public class MessageController {

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new MessageFormValidator());
    }

    @GetMapping("/send")
    public ModelAndView showMessageForm() {
        return new ModelAndView("sendMessage", "msgForm", new MessageForm());
    }

    @PostMapping("/send")
    public String sendMessageForm(@ModelAttribute("msgForm") @Valid MessageForm msgForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "sendMessage";
        }
        return "redirect:/message/sent";
    }

    @GetMapping("/sent")
    public String sentView() {
        return "sent";
    }

}
