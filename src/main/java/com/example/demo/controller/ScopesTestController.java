package com.example.demo.controller;

import com.example.demo.service.MySessionBean;
import com.example.demo.service.MyRequestBean;
import com.example.demo.service.MySingletonBean;
import com.example.demo.service.SomeOtherBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ScopesTestController {
    private final MySessionBean mySessionBean;
    private final MyRequestBean myRequestBean;
    private final MySingletonBean mySingletonBean;
    private final SomeOtherBean someOtherBean;

    public ScopesTestController(MySessionBean mySessionBean, MyRequestBean myRequestBean, MySingletonBean mySingletonBean, SomeOtherBean someOtherBean) {
        this.mySessionBean = mySessionBean;
        this.myRequestBean = myRequestBean;
        this.mySingletonBean = mySingletonBean;
        this.someOtherBean = someOtherBean;
    }

    @PostMapping("/scopeValues/session")
    public void setSession(@RequestParam("value") String value)  {
        this.mySessionBean.setSomeValue(value);
    }

    @PostMapping("/scopeValues/request")
    public void setRequest(@RequestParam("value") String value)  {
        this.myRequestBean.setSomeValue(value);
        someOtherBean.printValue();

    }

    @PostMapping("/scopeValues/singleton")
    public void setSingleton(@RequestParam("value") String value)  {
        this.mySingletonBean.setSomeValue(value);
    }

    @GetMapping("/scopeValues")
    public Map<String, String> getValue()  {
        String req = this.myRequestBean.getSomeValue();
        String session = this.mySessionBean.getSomeValue();
        String singleton = this.mySingletonBean.getSomeValue();

        Map<String, String> scopeValuesMap = new HashMap<>();
        scopeValuesMap.put("request", req);
        scopeValuesMap.put("session", session);
        scopeValuesMap.put("singleton", singleton);

        return scopeValuesMap;
    }
}
