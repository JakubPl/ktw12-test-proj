package com.example.demo.validator;

import com.example.demo.dto.MessageForm;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class MessageFormValidator implements Validator {
    private static final String EMAIL_NOT_GMAIL = "email.notGmail";

    @Override
    public boolean supports(Class<?> aClass) {
        return MessageForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MessageForm form = (MessageForm) o;
        if (!form.getEmail().endsWith("gmail.com")) {
            errors.rejectValue("email", EMAIL_NOT_GMAIL, "Not in gmail domain");
        }
    }
}
